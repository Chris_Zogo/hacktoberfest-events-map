export function randomGenerator(num: number) {
    const locations: LatLngs[] = [];
    for (let index = 0; index < num; index++) {
        //Latitude
        let min = -90;
        let max = 90;
        let lat = Number((Math.random() * (max - min + 1) + min).toFixed(15));
        //Longitude
        min = -180;
        max = 180;
        let lng = Number((Math.random() * (max - min + 1) + min).toFixed(15));
        locations.push({ lat: lat, lng: lng });
    }
    return locations;
}
export interface LatLngs {
    lat: number;
    lng: number;
}
